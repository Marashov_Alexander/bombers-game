//
// Created by alexander on 14.11.2019.
//

#include <stack>
#include <SFML/Audio.hpp>

#include "../headers/world.hpp"

World::World(sf::Vector2f position, float cageSize, int cagesCountX, int cagesCountY,
             int playersCount, int enemiesCount, sf::Font & font, int aiLevel) :
    cagesCountX_(cagesCountX),
    cagesCountY_(cagesCountY),
    cages_(),
    bombs_(),
    fires_(),
    items_(),
    players_(),
    enemies_(),
    labels_(),
    textureMap_(),
    soundsMap_(),
    music_(),
    isDangerous_(),
    font_(font),
    fontSize_()
    {


  fontSize_ = static_cast<int>(cageSize);

  textureMap_["cage"].loadFromFile("textures/cage.jpg");
  textureMap_["bomber"].loadFromFile("textures/player.png");
  textureMap_["explosion"].loadFromFile("textures/explosion.png");
  textureMap_["enemy"].loadFromFile("textures/skull.png");
  textureMap_["bomb"].loadFromFile("textures/bomb.png");
  textureMap_["bonus"].loadFromFile("textures/bonus.png");
  sprite_.setTexture(textureMap_["cage"]);

  soundsMap_["kill"].loadFromFile("sounds/kill.wav");
  soundsMap_["item"].loadFromFile("sounds/item.wav");
  soundsMap_["explosion"].loadFromFile("sounds/explosion.wav");

  music_.openFromFile("sounds/music.wav");
  music_.setLoop(true);
  music_.play();

  setPosition(position);
  setSize({cageSize * static_cast<float>(cagesCountX), cageSize * static_cast<float>(cagesCountY)});

  // создание клеток с заданием размеров и позиций с учётом выравнивания по центру
  for (int i = 0; i < cagesCountX_; ++i) {
    for (int j = 0; j < cagesCountY_; ++j) {
      const bool isWall = i == 0 || j == 0 || i == cagesCountX_ - 1 || j == cagesCountY_ - 1 || ((i % 2 == 0) && (j % 2 == 0));
      if (isWall) {
        cages_.push_back(
            std::make_shared<Cage>(
                static_cast<float>(i) * cageSize + position.x, static_cast<float>(j) * cageSize + position.y, cageSize,
                9999, true,
                textureMap_["cage"],
                sf::Color::Red
            )
        );
      } else {
//        int randomStrength = (seed % (i * j + 1) ? 1 : 0);
        int randomStrength = (rand() % 2);
        cages_.push_back(
            std::make_shared<Cage>(
                static_cast<float>(i) * cageSize + position.x, static_cast<float>(j) * cageSize + position.y, cageSize,
                randomStrength, randomStrength != 0,
                textureMap_["cage"],
                randomStrength == 0 ? sf::Color::White : sf::Color(255, 115, 0, 255)
            )
        );
      }
      isDangerous_.push_back(0);
    }
  }

  int freeCagesCount = ((cagesCountX - 6) / 2 + 1) * ((cagesCountY - 6) / 2 + 1) - enemiesCount;
  int enemiesRemains = enemiesCount;
  float delay = 5.f / static_cast<float>(aiLevel);

  for (int i = 3; i <= cagesCountX - 4; i += 2) {
    for (int j = 3; j <= cagesCountY - 4; j += 2) {
      const bool remains = rand() % 2 == 0;
      if ((remains || !freeCagesCount) && enemiesRemains) {
        --enemiesRemains;
        enemies_.push_back(std::make_shared<Enemy>(
            getCageCenter(i, j),
            cageSize,
            300.f,
            textureMap_["enemy"],
            sf::Color::White,
            delay
        ));

      } else {
        --freeCagesCount;
      }
    }
  }

  std::vector<sf::Vector2i> playersPositions = {
      sf::Vector2i(1, 1),
      sf::Vector2i(cagesCountX - 2, cagesCountY - 2),
      sf::Vector2i(1, cagesCountY - 2),
      sf::Vector2i(cagesCountX - 2, 1),
  };

  std::vector<sf::Color> playersColors = {
      sf::Color::White,
      sf::Color::Green,
      sf::Color::Yellow,
      sf::Color::Blue
  };

  std::vector<std::pair<DirectionKeysMap, ActionKeysMap>> playersControls = {
      std::make_pair<DirectionKeysMap, ActionKeysMap>(
          {
              {sf::Keyboard::Key::W, Direction::up},
              {sf::Keyboard::Key::A, Direction::left},
              {sf::Keyboard::Key::S, Direction::down},
              {sf::Keyboard::Key::D, Direction::right}
          },
          {
              {sf::Keyboard::Key::Q, Action::setBomb}
          }
      ),
      std::make_pair<DirectionKeysMap, ActionKeysMap>(
          {
              {sf::Keyboard::Key::Up, Direction::up},
              {sf::Keyboard::Key::Left, Direction::left},
              {sf::Keyboard::Key::Down, Direction::down},
              {sf::Keyboard::Key::Right, Direction::right}
          },
          {
              {sf::Keyboard::Key::RControl, Action::setBomb}
          }
      ),
      std::make_pair<DirectionKeysMap, ActionKeysMap>(
          {
              {sf::Keyboard::Key::Numpad8, Direction::up},
              {sf::Keyboard::Key::Numpad4, Direction::left},
              {sf::Keyboard::Key::Numpad5, Direction::down},
              {sf::Keyboard::Key::Numpad6, Direction::right}
          },
          {
              {sf::Keyboard::Key::Numpad7, Action::setBomb}
          }
      ),
      std::make_pair<DirectionKeysMap, ActionKeysMap>(
          {
              {sf::Keyboard::Key::U, Direction::up},
              {sf::Keyboard::Key::H, Direction::left},
              {sf::Keyboard::Key::J, Direction::down},
              {sf::Keyboard::Key::K, Direction::right}
          },
          {
              {sf::Keyboard::Key::Y, Action::setBomb}
          }
      ),
  };

  for (int i = 0; i < playersCount; ++i) {
    players_.push_back(
      std::make_shared<Player>(
          getCageCenter(playersPositions[i].x, playersPositions[i].y),
          cageSize,
          250.f,
          textureMap_["bomber"],
          playersColors[i],
          playersControls[i].first,
          playersControls[i].second,
          soundsMap_
      )
    );
  }
  int cagesSize = cagesCountX * cagesCountY;

  std::for_each(players_.begin(), players_.end(), [&cagesSize, this](PlayerPointer &personPointer) {
    int cageIndex = cageIndexOf(personPointer->getPosition());

    cages_[cageIndex]->setParams(0, false, sf::Color::White);
    if (cageIndex > 0 && !cages_[cageIndex - 1]->isUnbreakable()) {
      cages_[cageIndex - 1]->setParams(0, false, sf::Color::White);
    }
    if (cageIndex + 1 < cagesSize && !cages_[cageIndex + 1]->isUnbreakable()) {
      cages_[cageIndex + 1]->setParams(0, false, sf::Color::White);
    }
    if (cageIndex + cagesCountY_ < cagesSize && !cages_[cageIndex + cagesCountY_]->isUnbreakable()) {
      cages_[cageIndex + cagesCountY_]->setParams(0, false, sf::Color::White);
    }
    if (cageIndex - cagesCountY_ > 0 && !cages_[cageIndex - cagesCountY_]->isUnbreakable()) {
      cages_[cageIndex - cagesCountY_]->setParams(0, false, sf::Color::White);
    }

    if (cageIndex - 1 > 0 && !cages_[cageIndex - 2]->isUnbreakable()) {
      cages_[cageIndex - 2]->setParams(1, true, sf::Color(255, 115, 0, 255));
    }
    if (cageIndex + 2 < cagesSize && !cages_[cageIndex + 2]->isUnbreakable()) {
      cages_[cageIndex + 2]->setParams(1, true, sf::Color(255, 115, 0, 255));
    }
    if (cageIndex + 2 * cagesCountY_ < cagesSize && !cages_[cageIndex + 2 * cagesCountY_]->isUnbreakable()) {
      cages_[cageIndex + 2 * cagesCountY_]->setParams(1, true, sf::Color(255, 115, 0, 255));
    }
    if (cageIndex - 2 * cagesCountY_ > 0 && !cages_[cageIndex - 2 * cagesCountY_]->isUnbreakable()) {
      cages_[cageIndex - 2 * cagesCountY_]->setParams(1, true, sf::Color(255, 115, 0, 255));
    }

  });

  std::for_each(enemies_.begin(), enemies_.end(), [&cagesSize, this](EnemyPointer &enemyPointer) {
    const int cageIndex = cageIndexOf(enemyPointer->getPosition());
    cages_[cageIndex]->setParams(0, false, sf::Color::White);
  });
}

void World::dfs() {
  const int cagesSize = cagesCountY_ * cagesCountX_;
  bool isVisited[cagesSize];
  for (int i = 0; i < cagesSize; ++i) {
    isVisited[i] = false;
  }

  std::stack<int> stack;
  for (auto & player : players_) {
    if (!player->isAlive()) {
      continue;
    }
    int startIndex = cageIndexOf(player->getPosition());
    stack.push(startIndex);

    while (!stack.empty()) {

      int current = stack.top();
      stack.pop();
      if (isVisited[current]) {
        continue;
      }
      isVisited[current] = true;
      if (cages_[current]->isBarrier()) {
        continue;
      }

      int next = current + 1;
      if (next < cagesSize) {
        stack.push(next);
      }

      next = current - 1;
      if (next > 0) {
        stack.push(next);
      }

      next = current + cagesCountY_;
      if (next < cagesSize) {
        stack.push(next);
      }

      next = current - cagesCountY_;
      if (next > 0) {
        stack.push(next);
      }

    }
  }

  for (int i = 0; i < cagesSize; ++i) {
    cages_[i]->setVisible(isVisited[i]);
  }
  for (EnemyPointer & enemyPointer: enemies_) {
    enemyPointer->setVisible(cages_[cageIndexOf(enemyPointer->getPosition())]->isVisible());
  }

  for (ItemPointer & item: items_) {
    item->setVisible(cages_[cageIndexOf(item->getPosition())]->isVisible());
  }
}

Direction World::chooseDirection(int startIndex) {
  const int cagesSize = cagesCountY_ * cagesCountX_;
  int distance[cagesSize];
  for (int i = 0; i < cagesSize; ++i) {
    distance[i] = -1;
  }
  distance[startIndex] = 0;
  const bool allowDangerousCages = isDangerous_[startIndex];
  std::stack<int> stack;
  stack.push(startIndex);

  int purpose = startIndex;
  int minDistance = 0;

  while (!stack.empty()) {
    int current = stack.top();

    if (allowDangerousCages && (minDistance == 0 || minDistance > distance[current])) {
      minDistance = distance[current];
      purpose = current;
    } else if (minDistance < distance[current] && !isDangerous_[current]) {
      minDistance = distance[current];
      purpose = current;
    }
    stack.pop();

    int next = current + 1;
    if (next < cagesSize && !cages_[next]->isBarrier() && (!isDangerous_[next] || (isDangerous_[next] && allowDangerousCages))
        && (distance[next] == -1 || distance[next] > distance[current])) {
      distance[next] = distance[current] + 1;
      stack.push(next);
    }
    next = current - 1;
    if (next > 0 && !cages_[next]->isBarrier() && (!isDangerous_[next] || (isDangerous_[next] && allowDangerousCages))
        && (distance[next] == -1 || distance[next] > distance[current])) {
      distance[next] = distance[current] + 1;
      stack.push(next);
    }
    next = current + cagesCountY_;
    if (next < cagesSize && !cages_[next]->isBarrier() && (!isDangerous_[next] || (isDangerous_[next] && allowDangerousCages))
        && (distance[next] == -1 || distance[next] > distance[current])) {
      distance[next] = distance[current] + 1;
      stack.push(next);
    }
    next = current - cagesCountY_;
    if (next > 0 && !cages_[next]->isBarrier() && (!isDangerous_[next] || (isDangerous_[next] && allowDangerousCages))
        && (distance[next] == -1 || distance[next] > distance[current])) {
      distance[next] = distance[current] + 1;
      stack.push(next);
    }
  }

  for (PlayerPointer & personPointer: players_) {
    if (personPointer->isAlive()) {
      int index = cageIndexOf(personPointer->getPosition());
      if (distance[index] != -1 && distance[index] < minDistance && !isDangerous_[index]) {
        purpose = index;
        minDistance = distance[index];
      }
    }
  }

  Direction direction = none;
  int current = purpose;
  while (current != startIndex) {
    int next = current + 1;
    if (next < cagesSize && distance[next] != -1 && distance[next] < distance[current]) {
      current = next;
      direction = up;
    }
    next = current - 1;
    if (next > 0 && distance[next] != -1 && distance[next] < distance[current]) {
      current = next;
      direction = down;
    }
    next = current + cagesCountY_;
    if (next < cagesSize && distance[next] != -1 && distance[next] < distance[current]) {
      current = next;
      direction = left;
    }
    next = current - cagesCountY_;
    if (next > 0 && distance[next] != -1 && distance[next] < distance[current]) {
      current = next;
      direction = right;
    }
  }
  return direction;
}

sf::Vector2f World::getCageCenter(int cageIndexX, int cageIndexY) const {
  const int index = cageIndexX * cagesCountY_ + cageIndexY;
  sf::Vector2f center = cages_[index]->getPosition();
  center.x += cages_[index]->getSize().x / 2.f;
  center.y += cages_[index]->getSize().y / 2.f;
  return center;
}

sf::Vector2f World::getCageCenter(int index) const {
  sf::Vector2f center = cages_[index]->getPosition();
  center.x += cages_[index]->getSize().x / 2.f;
  center.y += cages_[index]->getSize().y / 2.f;
  return center;
}

void World::resize(sf::Vector2f position, sf::Vector2f size) {

  fontSize_ = static_cast<int>(size.x);

  std::for_each(players_.begin(), players_.end(), [&position, &size, this](PlayerPointer & personPointer) {
    const sf::Vector2f newPersonPosition = {
        (personPointer->getPosition().x - getPosition().x) * size.x *
            static_cast<float>(cagesCountX_) / getSize().x + position.x,
        (personPointer->getPosition().y - getPosition().y) * size.y *
            static_cast<float>(cagesCountY_) / getSize().y + position.y,
    };
    personPointer->resize(newPersonPosition, size);
  });

  std::for_each(enemies_.begin(), enemies_.end(), [&position, &size, this](EnemyPointer & enemyPointer) {
    const sf::Vector2f newPersonPosition = {
        (enemyPointer->getPosition().x - getPosition().x) * size.x *
            static_cast<float>(cagesCountX_) / getSize().x + position.x,
        (enemyPointer->getPosition().y - getPosition().y) * size.y *
            static_cast<float>(cagesCountY_) / getSize().y + position.y,
    };
    enemyPointer->resize(newPersonPosition, size);
  });

  setPosition(position);
  setSize({
              size.x * static_cast<float>(cagesCountX_),
              size.y * static_cast<float>(cagesCountY_)
          });
  for (int i = 0; i < cagesCountX_; ++i) {
    for (int j = 0; j < cagesCountY_; ++j) {
      cages_[i * cagesCountY_ + j]->resize(
          {
              static_cast<float>(i) * size.x + position.x,
                  static_cast<float>(j) * size.y + position.y
          },
          size
      );
    }
  }
  std::for_each(bombs_.begin(), bombs_.end(), [&size, this](const BombPointer &bomb) {
    bomb->resize(getCageCenter(bomb->getCageIndex()), size);
  });
  std::for_each(fires_.begin(), fires_.end(), [&size, this](const ItemPointer &itemPointer) {
    itemPointer->resize(getCageCenter(itemPointer->getCageIndex()), size);
  });

  std::for_each(items_.begin(), items_.end(), [&size, this](const ItemPointer &itemPointer) {
    itemPointer->resize(getCageCenter(itemPointer->getCageIndex()), size);
  });

  std::for_each(labels_.begin(), labels_.end(), [&size, this](const LabelPointer &label) {
    label->resize({}, {});
  });
}

void World::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  Actor::draw(target, states);
  std::for_each(
      cages_.begin(),
      cages_.end(),
      [&target, &states](const CagePointer &cage) {
        target.draw(*cage, states);
      }
  );
  std::for_each(
      bombs_.begin(),
      bombs_.end(),
      [&target, &states](const BombPointer &bomb) {
        target.draw(*bomb, states);
      }
  );

  std::for_each(
      players_.begin(),
      players_.end(),
      [&target, &states](const PlayerPointer &personPointer) {
        target.draw(*personPointer, states);
      }
  );

  std::for_each(
      enemies_.begin(),
      enemies_.end(),
      [&target, &states](const EnemyPointer &enemyPointer) {
        target.draw(*enemyPointer, states);
      }
  );

  std::for_each(
      fires_.begin(),
      fires_.end(),
      [&target, &states](const ItemPointer &item) {
        target.draw(*item, states);
      }
  );

  std::for_each(
      items_.begin(),
      items_.end(),
      [&target, &states](const ItemPointer &item) {
        target.draw(*item, states);
      }
  );

  std::for_each(
      labels_.begin(),
      labels_.end(),
      [&target, &states](const LabelPointer &label) {
        target.draw(*label, states);
      }
  );
//  target.draw(*enemiesRemainsLabel_, states);
}

void World::act(float delta) {
  dfs();

  std::for_each(players_.begin(), players_.end(), [&delta](PlayerPointer &person) {
    person->act(delta);
  });

  std::for_each(enemies_.begin(), enemies_.end(), [&delta, this](EnemyPointer &enemy) {
    enemy->act(delta);
    if (isUnderAttack(enemy->getPosition())) {
      enemy->hit(1);
    }
    if (enemy->needDirection() || enemy->getWallAhead() != none) {
      if (enemy->isAlive()) {
        enemy->setDirection(chooseDirection(cageIndexOf(enemy->getPosition())));
      } else {
        enemy->setDirection(none);
      }
    }
    std::for_each(players_.begin(), players_.end(), [&enemy, this](PlayerPointer & playerPointer) {
      if (cageIndexOf(playerPointer->getPosition()) == cageIndexOf(enemy->getPosition()) && enemy->isAlive()) {
        playerPointer->hit(1);
      }
    });
  });

  std::for_each(bombs_.begin(), bombs_.end(), [&delta](BombPointer &bomb) { bomb->act(delta); });
  std::for_each(fires_.begin(), fires_.end(), [&delta](ItemPointer &item) { item->act(delta); });
  std::for_each(items_.begin(), items_.end(), [&delta](ItemPointer &item) { item->act(delta); });
  bombs_.erase(
      std::remove_if(
          bombs_.begin(),
          bombs_.end(),
          [this](const BombPointer &pointer) {
            bool result = pointer->isActivated();
            if (result) {
              players_[pointer->getBomberIndex()]->bombExploded();
              const int cageIndex = cageIndexOf(pointer->getPosition());
              const int cagesSize = cagesCountX_ * cagesCountY_;
              int index = cageIndex;
              explode(index);

              bool a = true, b = true, c = true, d = true;

              for (int i = 1; i <= pointer->getRadius(); ++i) {
                index = cageIndex + i * cagesCountY_;
                if (index < cagesSize && a) {
                  if (cages_[index]->isBarrier()) {
                    a = false;
                  }
                  explode(index);
                }

                index = cageIndex - i * cagesCountY_;
                if (index > 0 && b) {
                  if (cages_[index]->isBarrier()) {
                    b = false;
                  }
                  explode(index);
                }

                index = cageIndex + i;
                if (index < (cageIndex / cagesCountY_ + 1) * cagesCountY_ && c) {
                  if (cages_[index]->isBarrier()) {
                    c = false;
                  }
                  explode(index);
                }

                index = cageIndex - i;
                if (index > (cageIndex / cagesCountY_) * cagesCountY_ && d) {
                  if (cages_[index]->isBarrier()) {
                    d = false;
                  }
                  explode(index);
                }
              }
            }
            return result;
          }
      ), bombs_.end()
  );

  fires_.erase(
      std::remove_if(fires_.begin(), fires_.end(), [this](ItemPointer &itemPointer) {
        bool result = itemPointer->isActivated();
        if (result) {
          isDangerous_[cageIndexOf(itemPointer->getPosition())] = 0;
        }
        return result;
      }),
      fires_.end()
  );

  items_.erase(
      std::remove_if(items_.begin(), items_.end(), [this](ItemPointer &itemPointer) { return itemPointer->isActivated(); }),
      items_.end()
  );
  checkItems();

  for (unsigned long i = 0; i < players_.size(); ++i) {
    PlayerPointer & personPointer = players_[i];
    Direction direction = personPointer->getDirection();
    if (direction != none && personPointer->getWallAhead() != direction) {
      personPointer->setWallAhead(none);
      sf::Vector2f newPosition = personPointer->getPosition();
      float step = delta * personPointer->getSpeed() * (cages_[0]->getScale().x);
      bool isX = false;

      switch (direction) {
        case up: {
          personPointer->setRotation(270);
          isX = false;
          step = -step;
          break;
        }
        case down: {
          personPointer->setRotation(90);
          isX = false;
          break;
        }
        case left: {
          personPointer->setRotation(180);
          isX = true;
          step = -step;
          break;
        }
        case right: {
          personPointer->setRotation(0);
          isX = true;
          break;
        }
        default: {

        }
      }
      if (toCorrectPosition(newPosition, step, isX)) {
        personPointer->setWallAhead(direction);
      }
      personPointer->setPosition(newPosition);
    }
    if (personPointer->getAction() == Action::setBomb && personPointer->hasBomb()) {
      setBomb(
          personPointer->getPosition(),
          std::make_shared<Bomb>(
              textureMap_["bomb"],
              personPointer->getPosition(),
              personPointer->getSize(),
              3.f,
              cageIndexOf(personPointer->getPosition()),
              personPointer->createBomb(), i
          )
      );
    }

    if (isUnderAttack(personPointer->getPosition())) {
      personPointer->hit(1);
    }
  }

  std::for_each(enemies_.begin(), enemies_.end(), [&delta, this](EnemyPointer & enemyPointer) {
    Direction direction = enemyPointer->getDirection();
    if (direction != none && enemyPointer->getWallAhead() != direction) {
      enemyPointer->setWallAhead(none);
      sf::Vector2f newPosition = enemyPointer->getPosition();
      float step = delta * enemyPointer->getSpeed() * cages_[0]->getScale().x;
      bool isX = false;

      switch (direction) {
        case up: {
          enemyPointer->setRotation(270);
          isX = false;
          step = -step;
          break;
        }
        case down: {
          enemyPointer->setRotation(90);
          isX = false;
          break;
        }
        case left: {
          enemyPointer->setRotation(180);
          isX = true;
          step = -step;
          break;
        }
        case right: {
          enemyPointer->setRotation(0);
          isX = true;
          break;
        }
        default: {

        }
      }
      if (toCorrectPosition(newPosition, step, isX)) {
        enemyPointer->setWallAhead(direction);
      }
      enemyPointer->setPosition(newPosition);
    }
  });

  std::for_each(labels_.begin(), labels_.end(), [&delta](LabelPointer & label) {
    label->act(delta);
  });

  labels_.erase(std::remove_if(labels_.begin(), labels_.end(), [](LabelPointer & label) {
    return !label->isAlive();
  }), labels_.end());
}

void World::explode(int index) {
  const bool isBomb = cages_[index]->isBomb();
  if (cages_[index]->hit(1)) {

    for (BombPointer & bombPointer: bombs_) {
      if (index == cageIndexOf(bombPointer->getPosition())) {
        bombPointer->activate();
        break;
      }
    }

//    if (seed_ % index < index / 3 && !isBomb) {
    if (rand() % 4 == 0 && !isBomb) {
      items_.push_back(std::make_shared<Item>(
          textureMap_["bonus"],
          getCageCenter(index),
          getCageSize(),
          20.f,
          cageIndexOf(getCageCenter(index))
      ));
    } else {
      fires_.push_back(std::make_shared<Item>(
          textureMap_["explosion"],
          getCageCenter(index),
          getCageSize(),
          1.f,
          cageIndexOf(getCageCenter(index)))
      );
    }
  } else {
    fires_.push_back(std::make_shared<Item>(
        textureMap_["explosion"],
        getCageCenter(index),
        getCageSize(),
        1.f,
        cageIndexOf(getCageCenter(index)))
    );
    for (ItemPointer & itemPointer: items_) {
      if (index == cageIndexOf(itemPointer->getPosition())) {
        itemPointer->activate();
        break;
      }
    }
  }
  isDangerous_[index] = false;
}

int World::cageIndexOf(const float x, const float y) const {
  const int iX = static_cast<int>((x - getPosition().x) / cages_[0]->getSize().x);
  const int iY = static_cast<int>((y - getPosition().y) / cages_[0]->getSize().y);
  return iX * cagesCountY_ + iY;
}

int World::cageIndexOf(const sf::Vector2f &position) const {
  const int iX = static_cast<int>((position.x - getPosition().x) / cages_[0]->getSize().x);
  const int iY = static_cast<int>((position.y - getPosition().y) / cages_[0]->getSize().y);
  return iX * cagesCountY_ + iY;
}

void World::setBomb(const sf::Vector2f &position, const BombPointer &bombPointer) {
  const int cageIndex = cageIndexOf(position.x, position.y);
  bombPointer->setPosition(getCageCenter(cageIndex));
  if (!cages_[cageIndex]->isBarrier()) {

    bombs_.push_back(bombPointer);
    cages_[cageIndex]->setParams(0, true, sf::Color::White);

    const int cagesSize = cagesCountX_ * cagesCountY_;
    int index = cageIndex;
    isDangerous_[index] = 1;

    bool a = true, b = true, c = true, d = true;

    for (int i = 1; i <= bombPointer->getRadius(); ++i) {
      index = cageIndex + i * cagesCountY_;
      if (index < cagesSize && a) {
        isDangerous_[index] = 1;
        if (cages_[index]->isBarrier()) {
          a = false;
        }
      }

      index = cageIndex - i * cagesCountY_;
      if (index > 0 && b) {
        isDangerous_[index] = 1;
        if (cages_[index]->isBarrier()) {
          b = false;
        }
      }

      index = cageIndex + i;
      if (index < (cageIndex / cagesCountY_ + 1) * cagesCountY_ && c) {
        isDangerous_[index] = 1;
        if (cages_[index]->isBarrier()) {
          c = false;
        }
      }

      index = cageIndex - i;
      if (index > (cageIndex / cagesCountY_) * cagesCountY_ && d) {
        isDangerous_[index] = 1;
        if (cages_[index]->isBarrier()) {
          d = false;
        }
      }

    }

  }
}

bool World::toCorrectPosition(sf::Vector2f &position, float delta, bool isX) {
  bool result = false;
  const float personSize = getCageSize().x;
  const float halfPersonSize = personSize / 2.f;

  sf::Vector2f controlPoint = {
      position.x + ((isX && delta > 0.00001f) ? (halfPersonSize - 1) : -halfPersonSize) + (isX ? delta : 0),
      position.y + ((!isX && delta > 0.00001f) ? (halfPersonSize - 1) : -halfPersonSize) + (!isX ? delta : 0)
  };
  const int cageIndex0 = cageIndexOf(controlPoint.x, controlPoint.y);
  const int currentCage = cageIndexOf(position);

  if (isX) {
    const int cageIndex1 = cageIndexOf(controlPoint.x, controlPoint.y - 1 + personSize);
    if (controlPoint.y - cages_[cageIndex0]->getPosition().y < halfPersonSize) {
      if (cages_[cageIndex0]->isBarrier() && cageIndex0 != currentCage) {
        // стоим
        result = true;
      } else if (cages_[cageIndex1]->isBarrier() && cageIndex1 != currentCage) {
        // |dx| -> -dy
        isX = false;
        delta = -std::abs(delta);
      } else {
        // dx
      }
    } else {
      if (cages_[cageIndex1]->isBarrier() && cageIndex1 != currentCage) {
        // стоим
        result = true;
      } else if (cages_[cageIndex0]->isBarrier() && cageIndex0 != currentCage) {
        // |dx| -> dy
        isX = false;
        delta = std::abs(delta);
      } else {
        // dx
      }
    }
  } else {
    const int cageIndex1 = cageIndexOf(controlPoint.x - 1 + personSize, controlPoint.y);
    if (cages_[cageIndex0]->isBarrier() || cages_[cageIndex1]->isBarrier())
      if (controlPoint.x - cages_[cageIndex0]->getPosition().x < halfPersonSize) {
        if (cages_[cageIndex0]->isBarrier() && cageIndex0 != currentCage) {
          // стоим
          result = true;
        } else if (cages_[cageIndex1]->isBarrier() && cageIndex1 != currentCage) {
          // |dy| -> -dx
          isX = true;
          delta = -std::abs(delta);
        } else {
          // dy
        }
      } else {

        if (cages_[cageIndex1]->isBarrier() && cageIndex1 != currentCage) {
          // стоим
          result = true;
        } else if (cages_[cageIndex0]->isBarrier() && cageIndex0 != currentCage) {
          // |dy| -> dx
          isX = true;
          delta = std::abs(delta);
        } else {
          // dy
        }

      }
  }

  sf::Vector2f topLeftPoint = {
      position.x - halfPersonSize + (isX ? delta : 0),
      position.y - halfPersonSize + (!isX ? delta : 0)
  };

  int cageIndex = cageIndexOf(topLeftPoint.x, topLeftPoint.y);
  if (cages_[cageIndex]->isBarrier() && currentCage != cageIndex) {
    // dx < 0 || dy < 0
    if (delta < -0.00001f) {
      if (isX) {
        topLeftPoint.x = cages_[cageIndex]->getPosition().x + cages_[cageIndex]->getSize().x;
      } else {
        topLeftPoint.y = cages_[cageIndex]->getPosition().y + cages_[cageIndex]->getSize().y;
      }
    }
  }

  cageIndex = cageIndexOf(topLeftPoint.x - 1 + personSize, topLeftPoint.y);
  if (cages_[cageIndex]->isBarrier() && currentCage != cageIndex) {
    // dx > 0 || dy < 0
    if (isX && delta > 0.00001f) {
      topLeftPoint.x = cages_[cageIndex]->getPosition().x - personSize;
    }
    if (!isX && delta < -0.00001f) {
      topLeftPoint.y = cages_[cageIndex]->getPosition().y + cages_[cageIndex]->getSize().y;
    }
  }

  cageIndex = cageIndexOf(topLeftPoint.x, topLeftPoint.y - 1 + personSize);
  if (cages_[cageIndex]->isBarrier() && currentCage != cageIndex) {
    // dx < 0 || dy > 0
    if (isX && delta < -0.00001f) {
      topLeftPoint.x = cages_[cageIndex]->getPosition().x + cages_[cageIndex]->getSize().x;
    }
    if (!isX && delta > 0.00001f) {
      topLeftPoint.y = cages_[cageIndex]->getPosition().y - personSize;
    }
  }

  cageIndex = cageIndexOf(topLeftPoint.x - 1 + personSize, topLeftPoint.y - 1 + personSize);
  if (cages_[cageIndex]->isBarrier() && currentCage != cageIndex) {
    // dx > 0 || dy > 0
    if (delta > 0.00001f) {
      if (isX) {
        topLeftPoint.x = cages_[cageIndex]->getPosition().x - personSize;
      } else {
        topLeftPoint.y = cages_[cageIndex]->getPosition().y - personSize;
      }
    }
  }
  position.x = topLeftPoint.x + halfPersonSize;
  position.y = topLeftPoint.y + halfPersonSize;

  return result;
}

bool World::isUnderAttack(sf::Vector2f position) const {
  const int cageIndex = cageIndexOf(position);
  for (const ItemPointer &fire: fires_) {

    if (fire->getCageIndex() == cageIndex && fire->getStatus() > 0.75f) {
      return true;
    }
  }
  return false;
}

sf::Vector2f World::getCageSize() const {
  return cages_[0]->getSize();
}

void World::onKeyPressed(sf::Keyboard::Key key) {
  std::for_each(players_.begin(), players_.end(), [&key](PlayerPointer & person) {
    person->onKeyPressed(key);
  });
}

void World::onKeyReleased(sf::Keyboard::Key key) {
  std::for_each(players_.begin(), players_.end(), [&key](PlayerPointer & person) {
    person->onKeyReleased(key);
  });
}

void World::checkItems() {
  std::for_each(players_.begin(), players_.end(), [this](PlayerPointer & person) {
    std::for_each(items_.begin(), items_.end(), [&person, this](ItemPointer & item) {
      if (person->isAlive() && cageIndexOf(person->getPosition()) == cageIndexOf(item->getPosition())) {
        std::string bonus;
        person->itemGot();
        if (rand() % 5 == 0) {
          person->addSpeed();
          bonus = "+speed";
        } else if (rand() % 3 == 0) {
          person->addBomb();
          bonus = "+light";
        } else {
          person->addPower();
          bonus = "+power";
        }
        labels_.push_back(std::make_shared<Label>(
            bonus, font_, fontSize_, person->getPosition(), false
        ));
        item->activate();
      }
    });
  });
}

int World::getPlayersCount() const {
  return std::count_if(players_.cbegin(), players_.cend(), [](const PlayerPointer & player) { return player->isAlive(); });
}

int World::getEnemiesCount() const {
  return std::count_if(enemies_.cbegin(), enemies_.cend(), [](const EnemyPointer & enemy) { return enemy->isAlive(); });
}
