//
// Created by alexander on 21.11.2019.
//

#include "../headers/label.hpp"


Label::Label(const std::string& message, sf::Font &font, int fontSize, sf::Vector2f position, bool isStatic):
  Actor(),
  text_(message, font, fontSize),
  isStatic_(isStatic)
{
//  text_.setOrigin(static_cast<float>(fontSize) / 2.f, static_cast<float>(fontSize) / 2.f);
  text_.setFillColor(sf::Color(0, 255, 0, 255));
  text_.setPosition(position);
}

void Label::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  target.draw(text_, states);
}

void Label::act(float delta) {
  if (lifeTime > 0.f && !isStatic_) {
    text_.setScale((1.5f - lifeTime / LABEL_LIFE_TIME), (1.5f - lifeTime / LABEL_LIFE_TIME));
    text_.setFillColor(sf::Color(0, 255, 0, 255 * lifeTime));
    lifeTime -= delta;
  }
}

void Label::resize(sf::Vector2f position, sf::Vector2f size) {
  text_.setPosition(position);
  text_.setCharacterSize(size.x);
}

void Label::setText(const std::string &string) {
  text_.setString(string);
}

bool Label::isAlive() const {
  return lifeTime > 0.f;
}
