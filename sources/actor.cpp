//
// Created by alexander on 16.11.2019.
//


#include <SFML/Graphics/Texture.hpp>

#include "../headers/actor.hpp"

Actor::Actor(const sf::Texture &texture) :
    sprite_(texture) {}

void Actor::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  target.draw(sprite_, states);
}

void Actor::setSize(const sf::Vector2f size) {
  const sf::Vector2u textureSize = sprite_.getTexture()->getSize();
  const float scaleX = static_cast<float>(size.x) / textureSize.x;
  const float scaleY = static_cast<float>(size.y) / textureSize.y;
  sprite_.setScale(scaleX, scaleY);
}

void Actor::resize(const sf::Vector2f position, const sf::Vector2f size) {
  sprite_.setPosition(position);
  setSize(size);
}

sf::Vector2f Actor::getSize() const {
  const sf::Vector2u textureSize = sprite_.getTexture()->getSize();
  const sf::Vector2f spriteScale = sprite_.getScale();
  return {textureSize.x * spriteScale.x, textureSize.y * spriteScale.y};
}

void Actor::setPosition(const sf::Vector2f position) {
  sprite_.setPosition(position);
}

sf::Vector2f Actor::getPosition() const {
  return sprite_.getPosition();
}

void Actor::act(float) {

}

void Actor::onKeyPressed(sf::Keyboard::Key) {

}

void Actor::onKeyReleased(sf::Keyboard::Key) {

}

sf::Vector2f Actor::getScale() const {
  return sprite_.getScale();
}

void Actor::setRotation(const float degrees) {
  sprite_.setRotation(degrees);
}

void Actor::setColor(sf::Color color) {
  sprite_.setColor(color);
}

void Actor::setTextureRect(sf::IntRect & rect) {
  sprite_.setTextureRect(rect);
}
