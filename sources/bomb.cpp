//
// Created by alexander on 18.11.2019.
//

#include "../headers/bomb.hpp"

Bomb::Bomb(sf::Texture &texture, sf::Vector2f position, sf::Vector2f size,
           float delay, int cageIndex, int radius, int bomberIndex) :
    Item::Item(texture, position, size, delay, cageIndex),
    radius_(radius),
    bomberIndex_(bomberIndex)
    {}

void Bomb::act(float delta) {
  sprite_.setColor(sf::Color(255 * (lifeTime_ / (timer_ + 0.1f)), 255 * (lifeTime_ / (timer_ + 0.1f)), 255 * (lifeTime_ / (timer_ + 0.1f)), 255));
  timer_ -= delta;
}

int Bomb::getRadius() const {
  return radius_;
}

int Bomb::getBomberIndex() const {
  return bomberIndex_;
}
