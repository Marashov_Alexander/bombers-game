//
// Created by alexander on 21.11.2019.
//

#include "../headers/player.hpp"

#include <utility>

Player::Player(sf::Vector2f position, float size, float speed, sf::Texture &texture, sf::Color color,
    DirectionKeysMap directionKeysMap, ActionKeysMap actionKeysMap, SoundsMap & soundsMap):

    Person(position, size, speed, texture, color),
    radius_(1),
    maxBombCount_(1),
    bombPlaced_(0),
    action_(nothing),
    actionKeys_(std::move(actionKeysMap)),
    moveKeys_(std::move(directionKeysMap)),
    killSound_(soundsMap["kill"]),
    itemSound_(soundsMap["item"]),
    bombSound_(soundsMap["explosion"])
{}

void Player::setAction(const sf::Keyboard::Key action) {
  if (actionKeys_.find(action) != actionKeys_.end()) {
    action_ = actionKeys_[action];
  }
}

void Player::setDirection(const sf::Keyboard::Key dir) {
  if (moveKeys_.find(dir) != moveKeys_.end()) {
    action_ = nothing;
    direction_ = moveKeys_[dir];
  }
}

void Player::clearDirection(const sf::Keyboard::Key dir) {
  if (moveKeys_.find(dir) != moveKeys_.end() && moveKeys_[dir] == direction_) {
    direction_ = none;
  }
}

void Player::onKeyPressed(sf::Keyboard::Key key) {
  setAction(key);
  setDirection(key);
}

void Player::onKeyReleased(sf::Keyboard::Key key) {
  clearDirection(key);
}

Action Player::getAction() const {
  return action_;
}

int Player::createBomb() {
  ++bombPlaced_;
  action_ = nothing;
  return radius_;
}

void Player::addPower() {
  ++radius_;
}

void Player::addBomb() {
  ++maxBombCount_;
}

void Player::hit(int power) {
  if (isAlive()) {
    moveKeys_.clear();
    actionKeys_.clear();
    killSound_.play();
  }
  Person::hit(power);
}

void Player::bombExploded() {
  --bombPlaced_;
  bombSound_.play();
}

bool Player::hasBomb() {
  return maxBombCount_ > bombPlaced_;
}

void Player::itemGot() {
  itemSound_.play();
}
