//
// Created by alexander on 18.11.2019.
//

#include "../headers/enemy.hpp"

Enemy::Enemy(sf::Vector2f position, const float size, const float speed, sf::Texture &texture, sf::Color color, float delay) :
    Person::Person(position, size, speed, texture, color),
    delay_(delay),
    timer_(delay)
    {}

void Enemy::act(float delta) {
  if (timer_ > 0) {
    timer_ -= delta;
  }
  Person::act(delta);
}

bool Enemy::needDirection() const {
  return timer_ < 0.f;
}

void Enemy::setDirection(Direction direction) {
  direction_ = direction;
  timer_ = delay_;
}

void Enemy::setVisible(bool visible) {
  isVisible_ = visible;
  sprite_.setColor(isVisible_ ? color_ : sf::Color::Transparent);
}
