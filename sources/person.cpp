//
// Created by alexander on 14.11.2019.
//

#include "../headers/person.hpp"

Person::Person(sf::Vector2f position, const float size, const float speed, sf::Texture &texture, sf::Color color):
    Actor(texture),
    speed_(speed),
    health_(1),
    direction_(none),
    wallAhead_(none),
    color_(color),
    isVisible_(true)
{
  sf::Vector2u tSize = texture.getSize();
  sprite_.setOrigin(tSize.x / 2.f, tSize.y / 2.f);
  resize(position, { size, size });
  sprite_.setColor(color);
}

void Person::hit(int power) {
  if (health_ > power) {
    health_ -= power;
  } else {
    health_ = 0;
    sprite_.setColor(sf::Color(128, 0, 0, 128));
    color_ = sf::Color(128, 0, 0, 0);
  }
}

Direction Person::getDirection() const {
  return direction_;
}

void Person::setWallAhead(Direction direction) {
  wallAhead_ = direction;
}

Direction Person::getWallAhead() const {
  return wallAhead_;
}

float Person::getSpeed() const {
  return speed_;
}

void Person::addSpeed() {
  speed_ += 50.f;
}
