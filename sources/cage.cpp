//
// Created by alexander on 14.11.2019.
//

#include "../headers/cage.hpp"
#include "../headers/actor.hpp"

Cage::Cage(float x, float y, float size, int strength, bool isBarrier, sf::Texture &background, sf::Color color):
    Actor(background),
    strength_(strength),
    isBarrier_(isBarrier),
    color_(color),
    isVisible_(false)
{
  resize({ x, y }, { size, size });
//  if (isBarrier) {
  sprite_.setColor(sf::Color(32, 32, 32, 255));
//  }
}

bool Cage::hit(const int power) {
  if (!isBarrier()) {
    return false;
  }
  if (power >= strength_) {
    strength_ = 0;
    isBarrier_ = false;
    sprite_.setColor(sf::Color::White);
    color_ = sf::Color::White;
  } else {
    strength_ -= power;
  }
  return !isBarrier();
}

void Cage::setParams(int strength, bool isBarrier, sf::Color color) {
  strength_ = strength;
  isBarrier_ = isBarrier;
  sprite_.setColor(color);
  color_ = color;
}

bool Cage::isUnbreakable() const {
  return strength_ > 1000;
}

bool Cage::isBarrier() const {
  return isBarrier_;
}

bool Cage::isBomb() const {
  return isBarrier_ && !strength_;
}

bool Cage::isVisible() const {
  return isVisible_;
}

void Cage::setVisible(bool visible) {
  isVisible_ = visible;
  sprite_.setColor(isVisible_ ? color_ : sf::Color(32, 32, 32, 255));
}
