//
// Created by alexander on 14.11.2019.
//

#include "../headers/item.hpp"
#include "../headers/actor.hpp"

Item::Item(sf::Texture &texture, sf::Vector2f position, sf::Vector2f size, float delay, int cageIndex):
    Actor::Actor(texture),
    lifeTime_(delay),
    timer_(delay),
    cageIndex_(cageIndex),
    isVisible_(true)
{
  sf::Vector2u tSize = texture.getSize();
  sprite_.setOrigin(tSize.x / 2.f, tSize.y / 2.f);
  resize(position, size);
}

void Item::act(float delta) {
  timer_ -= delta;
  if (isVisible()) {
    sprite_.setColor(sf::Color(255, 255, 255, 255 * (timer_ / lifeTime_)));
  }
}

void Item::activate() {
  timer_ = -0.0001f;
}

bool Item::isActivated() const {
  return timer_ < 0.f;
}

int Item::getCageIndex() const {
  return cageIndex_;
}

float Item::getStatus() const {
  return timer_ / lifeTime_;
}

bool Item::isVisible() const {
  return isVisible_;
}

void Item::setVisible(bool visible) {
  isVisible_ = visible;
  sprite_.setColor(isVisible_ ? sf::Color::White : sf::Color::Transparent);
}
