#include <SFML/Graphics.hpp>

#include "headers/world.hpp"

void updateLabelText(const char * prefix, std::string & text, const int count) {
  if (count) {
    text = prefix + std::to_string(count);
  } else {
    text = prefix;
  }
}

int main(int argc, char * argv[]) {

  const char * PLAYERS_REMAIN = "Miners left: ";
  const char * ENEMIES_REMAIN = "Bats left: ";
  const char * PLAYERS_DIED = "Bats won. Press esc to exit";
  const char * ENEMIES_DIED = "Miners won. Press esc to exit";

  const char * SIZE_X = "--size-x";
  const char * SIZE_Y = "--size-y";
  const char * PLAYERS_COUNT = "--players-count";
  const char * ENEMIES_COUNT = "--enemies-count";
  const char * SEED = "--seed";
  const char * AI_LEVEL = "--AI-level";

  const int cagesOffset = 2;
  std::map<std::string, int> parameters = {
      {SIZE_X, 13},
      {SIZE_Y, 9},
      {PLAYERS_COUNT, 2},
      {ENEMIES_COUNT, 1},
      {SEED, 53332400},
      {AI_LEVEL, 10}
  };

  for (int i = 1; i < argc; ++i) {
    std::string param = argv[i];
    int delimiterIndex = param.find('=');
    if (delimiterIndex == -1) {
      std::cerr << "Invalid flag: " << param << std::endl;
      return 1;
    }
    std::string key = param.substr(0, delimiterIndex);
    if (parameters.find(key) == parameters.end()) {
      std::cerr << "Unknown parameter: " << key << std::endl;
      return 1;
    }
    int value;
    try {
      value = std::stoi(param.substr(delimiterIndex + 1));
    }
    catch (std::invalid_argument &) {
      std::cerr << "Invalid value of the flag: " << param << std::endl;
      return 1;
    }
    parameters[key] = value;
  }

  const int cagesCountX = parameters[SIZE_X];
  const int cagesCountY = parameters[SIZE_Y];
  int playersCount = parameters[PLAYERS_COUNT];
  int enemiesCount = parameters[ENEMIES_COUNT];
  const int seed = parameters[SEED];
  const int aiLevel = parameters[AI_LEVEL];

  srand(seed);

  if (cagesCountX < 7 || cagesCountX > 33 || cagesCountY < 7 || cagesCountY > 33) {
    std::cerr << "Cages count must be between 7 and 33 " << std::endl;
    return 1;
  }
  if (cagesCountX % 2 == 0 || cagesCountY % 2 == 0) {
    std::cerr << "Cages count must be an odd number" << std::endl;
    return 1;
  }

  const int maxEnemiesCount = ((cagesCountX - 6) / 2 + 1) * ((cagesCountY - 6) / 2 + 1);
  const int maxPlayersCount = (cagesCountX < 9 || cagesCountY < 9) ? 2 : 4;
  const bool invalidPersonsCount = enemiesCount > maxEnemiesCount
      || playersCount > maxPlayersCount
      || playersCount < 1
      || enemiesCount < 0;
  if (invalidPersonsCount) {
    std::cerr << "Invalid persons count: the (" << cagesCountX << ";" << cagesCountY
        << ") world can contains enemies count between 0 and " << maxEnemiesCount
        << "and players count between 1 and " << maxPlayersCount << ".\n";
    return 1;
  }

  if (aiLevel > 50 || aiLevel < 1) {
    std::cerr << "AI level must be between 1 and 50" << std::endl;
    return 1;
  }

  int windowWidth = 600;
  int windowHeight = 400;

  const int minWindowWidth = windowWidth / 10;
  const int minWindowHeight = windowHeight / 10;

  float cageSize = std::min(windowWidth / (cagesCountX + cagesOffset), windowHeight / (cagesCountY + cagesOffset));
  sf::Vector2f worldPosition = {
      (static_cast<float>(windowWidth) - cageSize * static_cast<float>(cagesCountX)) / 2.f,
      (static_cast<float>(windowHeight) - cageSize * static_cast<float>(cagesCountY)) / 2.f
  };

  sf::Font font;
  font.loadFromFile("fonts/calibri.ttf");
  World & world = World::generateWorld(worldPosition, cageSize, cagesCountX, cagesCountY, playersCount, enemiesCount, font, aiLevel);

  std::string playersCountStr;
  std::string enemiesCountStr;
  updateLabelText(PLAYERS_REMAIN, playersCountStr, playersCount);
  updateLabelText(ENEMIES_REMAIN, enemiesCountStr, enemiesCount);

  Label playersRemainLabel(playersCountStr, font, static_cast<int>(cageSize),
      {cageSize, cageSize * 1.f / 3.f}, true);
  Label enemiesRemainLabel(enemiesCountStr, font, static_cast<int>(cageSize),
      {cageSize, static_cast<float>(windowHeight) - cageSize * 2.f / 3.f}, true);

  sf::Texture backgroundTexture;
  backgroundTexture.loadFromFile("textures/cage.jpg");
  backgroundTexture.setRepeated(true);

  Actor background(backgroundTexture);
  background.setPosition({ 0, 0 });

  sf::IntRect rect(0, 0, windowWidth, windowHeight);
  background.setTextureRect(rect);
  background.setColor(sf::Color(32, 32, 32, 255));

  sf::RenderWindow window(sf::VideoMode(windowWidth, windowHeight), "Bomberman");
  window.setKeyRepeatEnabled(false);

  sf::Clock clock;

  while (window.isOpen()) {

    const float delta = clock.getElapsedTime().asSeconds();
    clock.restart();

    sf::Event event{};
    while (window.pollEvent(event)) {

      if (event.type == sf::Event::Closed) {
        window.close();
      }

      if (event.type == sf::Event::Resized) {
        int newWidth = static_cast<int>(event.size.width);
        int newHeight = static_cast<int>(event.size.height);

        if (newWidth < minWindowWidth || newHeight < minWindowHeight) {
          window.setSize({static_cast<unsigned int>(std::max(minWindowWidth, newWidth)), static_cast<unsigned int>(std::max(minWindowHeight, newHeight))});
          break;
        }

        window.setView(sf::View(sf::FloatRect(0, 0, newWidth, newHeight)));

        cageSize = std::min(newWidth / (cagesCountX + cagesOffset), newHeight / (cagesCountY + cagesOffset));
        worldPosition = {
            (static_cast<float>(newWidth) - cageSize * static_cast<float>(cagesCountX)) / 2.f,
            (static_cast<float>(newHeight) - cageSize * static_cast<float>(cagesCountY)) / 2.f
        };
        world.resize(worldPosition, {cageSize, cageSize});
        rect = {0, 0, newWidth, newHeight};
        background.setPosition({0, 0});
        background.setTextureRect(rect);

        playersRemainLabel.resize({cageSize, 0}, {cageSize, cageSize});
        enemiesRemainLabel.resize({cageSize, static_cast<float>(newHeight) - cageSize * 4 / 3}, {cageSize, cageSize});
      }

      if (event.type == sf::Event::KeyPressed) {
        world.onKeyPressed(event.key.code);
        if (event.key.code == sf::Keyboard::Escape) {
          return 0;
        }
      }

      if (event.type == sf::Event::KeyReleased) {
        world.onKeyReleased(event.key.code);
      }
    }

    world.act(delta);

    window.clear();

    window.draw(background);
    window.draw(world);

    int newPlayersCount = world.getPlayersCount();
    int newEnemiesCount = world.getEnemiesCount();
    if (newPlayersCount != playersCount) {
      updateLabelText(newPlayersCount ? PLAYERS_REMAIN : PLAYERS_DIED, playersCountStr, newPlayersCount);
      playersRemainLabel.setText(playersCountStr);
      playersCount = newPlayersCount;
    }
    if (newEnemiesCount != enemiesCount) {
      updateLabelText(newEnemiesCount ? ENEMIES_REMAIN : ENEMIES_DIED, enemiesCountStr, newEnemiesCount);
      enemiesRemainLabel.setText(enemiesCountStr);
      enemiesCount = newEnemiesCount;
    }

    window.draw(playersRemainLabel);
    window.draw(enemiesRemainLabel);

    window.display();
  }

  /*
   * 1) отправить на почту полноценный отчёт
   * 2) + титульный лист принести для подписи
   * 3) + рабочая программа
   * */
  return 0;
}
