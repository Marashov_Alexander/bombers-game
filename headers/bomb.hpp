//
// Created by alexander on 18.11.2019.
//

#ifndef BOMBERS_BOMB_HPP
#define BOMBERS_BOMB_HPP

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Audio.hpp>

#include "item.hpp"

class Bomb: public Item {
public:

  Bomb(sf::Texture & texture, sf::Vector2f position, sf::Vector2f size, float delay,
      int cageIndex, int radius, int bomberIndex);

  ~Bomb() override = default;

  void act(float delta) override;

  // получить радиус действия бомбы
  int getRadius() const;

  // получить номер игрока, который поставил бомбу
  int getBomberIndex() const;

private:
  // радиус действия
  int radius_;

  // игрок, поставивший бомбу
  int bomberIndex_;
};


#endif //BOMBERS_BOMB_HPP
