//
// Created by alexander on 21.11.2019.
//

#ifndef BOMBERS_PLAYER_HPP
#define BOMBERS_PLAYER_HPP


#include <SFML/Audio.hpp>
#include "person.hpp"

enum Action {
  setBomb,
  nothing
};

using SoundsMap = std::map<std::string, sf::SoundBuffer>;
using DirectionKeysMap = std::map<sf::Keyboard::Key, Direction>;
using ActionKeysMap = std::map<sf::Keyboard::Key, Action>;

class Player: public Person {
public:
  Player(sf::Vector2f position, float size, float speed, sf::Texture &texture, sf::Color color,
      DirectionKeysMap directionKeysMap, ActionKeysMap actionKeysMap, SoundsMap & soundsMap);

  void onKeyPressed(sf::Keyboard::Key key) override;

  void onKeyReleased(sf::Keyboard::Key key) override;

  void hit(int power) override;


  // задать/получить текущее действие
  void setAction(sf::Keyboard::Key action);
  Action getAction() const;

  // установить/сбросить направление
  void setDirection(sf::Keyboard::Key dir);
  void clearDirection(sf::Keyboard::Key dir);

  // есть ли доступные бомбы
  bool hasBomb();

  // создать бомбу
  int createBomb();

  // вернуть в количество свободных бомб единицу
  void bombExploded();

  // увеличить силу бомб
  void addPower();

  // увеличить количество доступных бомб
  void addBomb();

  // получен предмет
  void itemGot();

private:
  int radius_;
  int maxBombCount_;
  int bombPlaced_;

  Action action_;

  // представление клавиш управления в соответствующие действия/передвижения
  std::map<sf::Keyboard::Key, Action> actionKeys_;
  std::map<sf::Keyboard::Key, Direction> moveKeys_;

  sf::Sound killSound_;
  sf::Sound itemSound_;
  sf::Sound bombSound_;
};


#endif //BOMBERS_PLAYER_HPP
