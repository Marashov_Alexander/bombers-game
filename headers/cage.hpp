//
// Created by alexander on 14.11.2019.
//

#ifndef BOMBERS_CAGE_HPP
#define BOMBERS_CAGE_HPP


#include <iostream>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "item.hpp"
#include "actor.hpp"

class Cage: public Actor {
public:
  Cage(float x, float y, float size,  int strength, bool isBarrier, sf::Texture & background, sf::Color color);

  // ударить препятствие с силой power
  bool hit(int power);

  // задать параметры клетки
  void setParams(int strength, bool isBarrier, sf::Color color);

  // является ли клетка непробиваемой стеной
  bool isUnbreakable() const;

  // является ли клетка препятствием
  bool isBarrier() const;

  // является ли клетка препятствием из-за установленной на ней бомбы
  bool isBomb() const;

  // видима ли информация клетки для игроков
  bool isVisible() const;

  // установить видимость информации клетки
  void setVisible(bool visible);

private:
  // прочность препятствия - при нуле isBarrier_ автоматически устанавливается в 0
  int strength_;
  // является ли преградой
  bool isBarrier_;
  // цвет клетки
  sf::Color color_;
  // видимость информации
  bool isVisible_;
};


#endif //BOMBERS_CAGE_HPP
