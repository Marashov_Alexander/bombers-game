//
// Created by alexander on 16.11.2019.
//

#ifndef BOMBERS_ACTOR_HPP
#define BOMBERS_ACTOR_HPP


#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Window/Keyboard.hpp>

class Actor : public sf::Drawable {
public:
  Actor() = default;

  explicit Actor(const sf::Texture &texture);

  ~Actor() override = default;

  // отрисовать элемент
  void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

  // обработать логику элемента. delta - время, прошедшее с прошлого вызова метода
  virtual void act(float delta);

  // обработать событие нажатия клавиши
  virtual void onKeyPressed(sf::Keyboard::Key key);

  // обработать событие отпускания клавиши
  virtual void onKeyReleased(sf::Keyboard::Key key);

  // изменить свой размер и позицию
  virtual void resize(sf::Vector2f position, sf::Vector2f size);

  // установить размер элемента
  virtual void setSize(sf::Vector2f size);

  // установить поворот
  virtual void setRotation(float degrees);

  // получить размер элемента
  virtual sf::Vector2f getSize() const;

  // установить позицию элемента
  virtual void setPosition(sf::Vector2f position);

  // получить позицию элемента
  virtual sf::Vector2f getPosition() const;

  // получить коэффициенты масштабирования относительно текстуры
  virtual sf::Vector2f getScale() const;

  // установить цвет спрайта
  virtual void setColor(sf::Color color);

  // установить прямоугольник отображения спрайта из текстуры
  virtual void setTextureRect(sf::IntRect & rect);

protected:
  sf::Sprite sprite_;
};


#endif //BOMBERS_ACTOR_HPP
