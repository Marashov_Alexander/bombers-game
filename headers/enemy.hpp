//
// Created by alexander on 18.11.2019.
//

#ifndef BOMBERS_ENEMY_HPP
#define BOMBERS_ENEMY_HPP


#include "person.hpp"

class Enemy: public Person {
public:
  Enemy(sf::Vector2f position, float size, float speed, sf::Texture & texture, sf::Color color, float delay);

  void act(float delta) override;

  // требуется ли пересчёт направления движения
  bool needDirection() const;

  // установить направление движения
  void setDirection(Direction direction);

  // установить видимость для игроков
  void setVisible(bool visible);

private:
  // задержка пересчёта пути - определяет уровень интеллекта врага
  float delay_;

  // таймер пересчёта пути
  float timer_;
};


#endif //BOMBERS_ENEMY_HPP
