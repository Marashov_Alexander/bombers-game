//
// Created by alexander on 21.11.2019.
//

#ifndef BOMBERS_LABEL_HPP
#define BOMBERS_LABEL_HPP


#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include "actor.hpp"

const float LABEL_LIFE_TIME = 1.f;

class Label: public Actor {
public:
  Label(const std::string& message, sf::Font & font, int fontSize, sf::Vector2f position, bool isStatic);

  void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

  void act(float delta) override;

  void resize(sf::Vector2f position, sf::Vector2f size) override;

  // установить новый текст
  void setText(const std::string & string);

  // не вышло ли время жизни текста
  bool isAlive() const;

private:
  sf::Text text_;
  // время жизни текста. По умолчанию, 1 секунда
  float lifeTime = LABEL_LIFE_TIME;
  // является ли текст постоянным
  bool isStatic_;
};


#endif //BOMBERS_LABEL_HPP
