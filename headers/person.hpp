//
// Created by alexander on 14.11.2019.
//

#ifndef BOMBERS_PERSON_HPP
#define BOMBERS_PERSON_HPP


#include <map>
#include <memory>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Window/Keyboard.hpp>

#include "actor.hpp"
#include "bomb.hpp"

// направление движения
enum Direction {
  up = 0,
  down,
  right,
  left,
  none
};

class Person: public Actor {
public:

  Person(sf::Vector2f position, float size, float speed, sf::Texture & texture, sf::Color color);

  // ударить игрока с силой power
  virtual void hit(int power);

  // получить текущее направление
  Direction getDirection() const;

  // запомнить стену в направлении direction
  void setWallAhead(Direction direction);

  // получить направление, в котором точно стена
  Direction getWallAhead() const;

  // получить скорость игрока
  float getSpeed() const;

  // жив ли персонаж
  bool isAlive() const {
    return health_ > 0;
  }

  // добавить очко скорости
  void addSpeed();

protected:
  // скорость передвижения в секунду
  float speed_;

  // количество жизней персонажа
  int health_;

  // текущее направление
  Direction direction_;

  // направление, в котором находится стена. Поле для оптимизации
  Direction wallAhead_;

  sf::Color color_;
  bool isVisible_;
};


#endif //BOMBERS_PERSON_HPP
