//
// Created by alexander on 14.11.2019.
//

#ifndef BOMBERS_ITEM_HPP
#define BOMBERS_ITEM_HPP


#include <SFML/Graphics/Texture.hpp>
#include <SFML/Audio.hpp>
#include <iostream>

#include "actor.hpp"

class Item: public Actor {
public:

  Item(sf::Texture & texture, sf::Vector2f position, sf::Vector2f size, float delay, int cageIndex);

  void act(float delta) override;

  // досрочно сбросить таймер
  void activate();

  // вышло ли время на таймере
  bool isActivated() const;

  // получить номер клетки, на которой установлен
  int getCageIndex() const;

  // получить отношение оставшегося времени таймера к начальному значению
  float getStatus() const;

  // получить видимость предмета
  bool isVisible() const;

  // устанавливает видимость предмета
  void setVisible(bool visible);

protected:

  // время жизни предмета
  float lifeTime_;
  // оставшееся время жизни
  float timer_;
  // номер клетки, на которой стоит
  int cageIndex_;
  // видимость предмета
  bool isVisible_;
};


#endif //BOMBERS_ITEM_HPP
