//
// Created by alexander on 14.11.2019.
//

#ifndef BOMBERS_WORLD_HPP
#define BOMBERS_WORLD_HPP


#include <vector>
#include <list>
#include <memory>
#include <cmath>

#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

#include "cage.hpp"
#include "actor.hpp"
#include "bomb.hpp"
#include "enemy.hpp"
#include "player.hpp"
#include "label.hpp"

using TextureMap = std::map<std::string, sf::Texture>;

using CagePointer = std::shared_ptr<Cage>;
using ItemPointer = std::shared_ptr<Item>;
using PlayerPointer = std::shared_ptr<Player>;
using EnemyPointer = std::shared_ptr<Enemy>;
using BombPointer = std::shared_ptr<Bomb>;
using LabelPointer = std::shared_ptr<Label>;

class World : public Actor {
public:

  // копирование экземпляра запрещено
  World(World &) = delete;

  World &operator=(World &) = delete;

  // класс является синглтоном
  static World &generateWorld(sf::Vector2f position, float cageSize, int cagesCountX, int cagesCountY,
                              int playersCount, int enemiesCount, sf::Font & font, int aiLevel) {

    static World world(position, cageSize, cagesCountX, cagesCountY, playersCount, enemiesCount, font, aiLevel);
    return world;

  }

  sf::Vector2f getCageSize() const;
  sf::Vector2f getCageCenter(int cageIndexX, int cageIndexY) const;
  sf::Vector2f getCageCenter(int index) const;

  void resize(sf::Vector2f position, sf::Vector2f size) override;
  void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
  void act(float delta) override;
  void onKeyPressed(sf::Keyboard::Key key) override;
  void onKeyReleased(sf::Keyboard::Key key) override;

  int getPlayersCount() const;
  int getEnemiesCount() const;

private:
  // конструктор и деструктор скрыты
  World(sf::Vector2f position, float cageSize, int cagesCountX, int cagesCountY,
        int playerCount, int enemyCount, sf::Font & font, int aiLevel);

  ~World() override = default;

  int cageIndexOf(float x, float y) const;

  int cageIndexOf(const sf::Vector2f &position) const;

  void setBomb(const sf::Vector2f &position, const BombPointer &bombPointer);

  bool toCorrectPosition(sf::Vector2f &position, float delta, bool isX);

  bool isUnderAttack(sf::Vector2f position) const;

  Direction chooseDirection(int startIndex);
  void explode(int index);
  void checkItems();
  void dfs();

  // требуемое количество клеток по ширине и по высоте
  int cagesCountX_;
  int cagesCountY_;

  // информация про игровое поле
  std::vector<CagePointer> cages_;
  std::list<BombPointer> bombs_;
  std::list<ItemPointer> fires_;
  std::list<ItemPointer> items_;
  std::vector<PlayerPointer> players_;
  std::list<EnemyPointer> enemies_;
  std::list<LabelPointer> labels_;

  TextureMap textureMap_;
  SoundsMap soundsMap_;
  sf::Music music_;

  std::vector<int> isDangerous_;

  sf::Font font_;
  int fontSize_;
};

#endif //BOMBERS_WORLD_HPP
